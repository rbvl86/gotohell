﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.CorgiEngine;
using MoreMountains.Feedbacks;

public class ReloadDash : MonoBehaviour
{

    public MMFeedbacks feedback;

    void Awake()
    {
        feedback?.Initialization(this.gameObject);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.GetComponent<CharacterDamageDash>().ResetCooldown();

            BoxCollider2D col = GetComponent<BoxCollider2D>();
            Transform t = transform.Find("Gem");

            col.enabled = false;
            t.localScale = Vector3.zero;

            feedback?.PlayFeedbacks(this.transform.position);

            StartCoroutine(ReactivateReloadDash(col, t));
        }
    }

    IEnumerator ReactivateReloadDash(BoxCollider2D col, Transform t)
    {
        yield return new WaitForSeconds(2);

        col.enabled = true;
        t.localScale = new Vector3(1, 1, 1);
    }
}
