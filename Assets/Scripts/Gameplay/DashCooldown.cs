﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.Tools;
using UnityEngine.UI;

namespace MoreMountains.CorgiEngine
{
    public class DashCooldown : CharacterAbility
    {
        private float endTimestamp = 0f;
        private float totalCooldownTime;

        public void StartCooldown(float endTs)
        {
            endTimestamp = endTs;
            totalCooldownTime = _character.GetComponent<CharacterDamageDash>().DashCooldown;
        }

        // Update is called once per frame
        void Update()
        {
            if (endTimestamp == 0f)
                return;
            Image cooldownBar = GameObject.Find("DashCooldownBar").GetComponent<Image>();
            float timeLeft = endTimestamp - Time.time;
            float cooldownLeftPercent = (timeLeft * 100 / totalCooldownTime);
            cooldownBar.fillAmount = cooldownLeftPercent / 100;
        }
    }
}
