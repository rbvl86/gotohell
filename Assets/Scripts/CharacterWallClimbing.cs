﻿using UnityEngine;
using System.Collections;
using MoreMountains.Tools;

namespace MoreMountains.CorgiEngine // you might want to use your own namespace here
{
	/// <summary>
	/// DESCRIPTION
	/// </summary>
	[AddComponentMenu("Corgi Engine/Character/Abilities/CharacterClimbWalls")]
	public class CharacterWallClimbing : CharacterAbility
	{
		/// This method is only used to display a helpbox text
		/// at the beginning of the ability's inspector
		public override string HelpBoxText() { return "This ability allows the character to climb up if the WallClinging is true and Input is up."; }

		[Header("Climbing parameters")]
		/// declare your parameters here
		public float climbSpeed = 4f;
		public float gracePeriod = .2f;

		private float gracePeriodTimer;
		private bool isWallClimbing;

		// animation parameters
		protected const string _wallClimbingAnimationParameterName = "WallClimbing";
		protected int _wallClimbingAnimationParameter;

		/// <summary>
		/// Here you should initialize our parameters
		/// </summary>
		protected override void Initialization()
		{
			base.Initialization();
			gracePeriodTimer = gracePeriod;
		}

		/// <summary>
		/// Every frame, we check if we're crouched and if we still should be
		/// </summary>
		public override void ProcessAbility()
		{
			base.ProcessAbility();
			if (_movement.CurrentState == CharacterStates.MovementStates.WallClinging)
            {
				HandleWallClimbing();
            }
            if ((_character.IsFacingRight && _horizontalInput <= -1) || (!_character.IsFacingRight && _horizontalInput >= 1))
            {
				DecrementGracePeriod();
			}
            if (gracePeriodTimer <= 0)
            {
				StopWallClimbing();
			}
            if (_movement.CurrentState != CharacterStates.MovementStates.WallClinging && _movement.PreviousState == CharacterStates.MovementStates.WallClinging)
            {
				StopWallClimbing();
				ResetGracePeriod();
            }
		}

        void DecrementGracePeriod()
        {
			gracePeriodTimer -= .1f;
        }

        void ResetGracePeriod()
        {
			gracePeriodTimer = gracePeriod;
        }

		/// <summary>
		/// Called at the start of the ability's cycle, this is where you'll check for input
		/// </summary>
		protected override void HandleInput()
		{
			// here as an example we check if we're pressing up or down
			// on our main stick/direction pad/keyboard
			
		}

		/// <summary>
		/// If we're pressing down, we check for a few conditions to see if we can perform our action
		/// </summary>
		protected virtual void HandleWallClimbing()
		{
			// if the ability is not permitted
			if (!AbilityPermitted
				// or if we're not in our normal stance
				|| (_condition.CurrentState != CharacterStates.CharacterConditions.Normal)
				// or if we're grounded
				|| (_controller.State.IsGrounded)
				// or if we're not WallClinging
				|| (_movement.CurrentState != CharacterStates.MovementStates.WallClinging))
			{
				// we do nothing and exit
				return;
			}

			_character.GetComponent<CharacterHorizontalMovement>().AbilityPermitted = false;
			_controller.GravityActive(false);
			_controller.SetVerticalForce(_verticalInput * climbSpeed);
		}

        protected virtual void StopWallClimbing()
        {
			_movement.ChangeState(CharacterStates.MovementStates.Idle);
			_character.GetComponent<CharacterHorizontalMovement>().AbilityPermitted = true;
			_controller.GravityActive(true);
		}

		/// <summary>
		/// Adds required animator parameters to the animator parameters list if they exist
		/// </summary>
		protected override void InitializeAnimatorParameters()
		{
            RegisterAnimatorParameter(_wallClimbingAnimationParameterName, AnimatorControllerParameterType.Bool, out _wallClimbingAnimationParameter);
		}

		/// <summary>
		/// At the end of the ability's cycle,
		/// we send our current crouching and crawling states to the animator
		/// </summary>
		public override void UpdateAnimator()
		{

		}
	}
}
