﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxFrame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        foreach(Transform child in transform)
        {
            GameObject gameObject = child.gameObject;
            string name = gameObject.name;

            ParallaxEffect parallaxEffect;

            if (gameObject.GetComponent<ParallaxEffect>() == null)
            {
                parallaxEffect = gameObject.AddComponent(typeof(ParallaxEffect)) as ParallaxEffect;
            } else
            {
                parallaxEffect = gameObject.GetComponent<ParallaxEffect>();
            }

            switch(name)
            {
                case "BG":
                    parallaxEffect.SetParallaxEffectAmount(1f);
                    break;
                case "MG1":
                    parallaxEffect.SetParallaxEffectAmount(.8f);
                    break;
                case "MG2":
                    parallaxEffect.SetParallaxEffectAmount(.6f);
                    break;
                case "MG3":
                    parallaxEffect.SetParallaxEffectAmount(.2f);
                    break;
                case "FG":
                    parallaxEffect.SetParallaxEffectAmount(-.3f);
                    break;
                default:
                    parallaxEffect.SetParallaxEffectAmount(0f);
                    break;
            }
        }        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
