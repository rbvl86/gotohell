﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxEffect : MonoBehaviour
{
    private float startpos;
    private GameObject cam;
    float parallaxEffectAmount;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main.gameObject;
        startpos = transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        float dist = (cam.transform.position.x * parallaxEffectAmount);

        transform.position = new Vector3(startpos + dist, transform.position.y, transform.position.z);
    }

    public void SetParallaxEffectAmount(float amount)
    {
        parallaxEffectAmount = amount;
    }
}
