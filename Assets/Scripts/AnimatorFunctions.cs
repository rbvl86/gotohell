﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorFunctions : MonoBehaviour
{
    private AudioSource AudioSource;

    void Start()
    {
        AudioSource = GetComponent<AudioSource>();
    }

    void PlayStep()
    {
        AudioSource.pitch = Random.Range(.8f, 1.2f);
        AudioSource.Play();
    }
}
